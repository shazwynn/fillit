/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_tetris.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triou <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 17:05:27 by triou             #+#    #+#             */
/*   Updated: 2018/04/09 20:48:05 by triou            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_convert_tetris(t_tetris *begin_list)
{
	t_tetris	*list;
	int			i;

	i = 0;
	list = begin_list;
	while (list)
	{
		i = 0;
		while (i < 4)
		{
			list->tetris[i][0] = list->shape[i] / 4 - list->shape[0] / 4;
			list->tetris[i][1] = list->shape[i] % 4 - list->shape[0] % 4;
			i++;
		}
		list = list->next;
	}
}
