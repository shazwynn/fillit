#include "fillit.h"

t_tetris	*ft_create_elem(int	ntetris)
{
	t_tetris *elem;

	if (!(elem = malloc(sizeof(*elem))))
		return (NULL);
	elem->order = ntetris;
	elem->shape[0] = 0;
	elem->next = NULL;
	return (elem);
}
