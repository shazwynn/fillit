#include "fillit.h"

static char	*ft_strnjoin(char *file, char *buff, int ret)
{
	char	*new;
	size_t	len;

	len = 0;
	if (file)
		len = ft_strlen(file);
	if (!(new = (char *)malloc(sizeof(*new) * (len + ret + 1))))
		return (NULL);
	*new = '\0';
	if (file)
	{
		ft_strcpy(new, file);
		free(file);
	}
	return (ft_strncat(new, buff, ret));
}

char		*ft_read_file(int fd)
{
	char	*file;
	char	buff[BUFF_SIZE];
	int		ret;

	file = NULL;
	while ((ret = read(fd, buff, BUFF_SIZE)) > 0)
		file = ft_strnjoin(file, buff, ret);
	if (ret < 0)
	{
		free(file);
		return (NULL);
	}
	return (file);
}
