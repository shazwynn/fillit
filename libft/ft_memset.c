/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 17:04:32 by algrele           #+#    #+#             */
/*   Updated: 2018/04/06 17:04:39 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char *count;

	if (len == 0)
		return (b);
	count = (unsigned char *)b;
	while (len > 0)
	{
		*count = (unsigned char)c;
		count++;
		len--;
	}
	return (b);
}
