/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 18:07:46 by algrele           #+#    #+#             */
/*   Updated: 2018/04/06 20:38:30 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static void	rec_itoa_base(long int value, int base, char *res)
{
	int	i;

	i = 0;
	if (value < 0)
	{
		if (base == 10)
			res[0] = '-';
		value = -value;
	}
	if (value >= base)
	{
		rec_itoa_base(value / base, base, res);
		rec_itoa_base(value % base, base, res);
	}
	if (value < base)
	{
		while (res[i])
			i++;
		if (value <= 9)
			res[i] = value + 48;
		if (value >= 10)
			res[i] = value + 55;
	}
}

static int	ft_count_itoa(long int value, int base)
{
	int	count;

	count = 0;
	if (value < 0 && base == 10)
	{
		count++;
		value = -value;
	}
	if (value == 0)
		count++;
	while (value > 0)
	{
		value = value / base;
		count++;
	}
	return (count);
}

char		*ft_itoa_base(int value, int base)
{
	char	*res;
	int		count;

	count = ft_count_itoa(value, base);
	res = ft_strnew(count);
	if (!res || base < 2 || base > 16)
		return (NULL);
	rec_itoa_base(value, base, res);
	return (res);
}
