/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 22:36:09 by algrele           #+#    #+#             */
/*   Updated: 2018/04/07 00:54:39 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stdlib.h>

# define TRUE 1
# define FALSE 0
# define LOWCASE 0
# define UPCASE 1
# define NUMCASE 2

# define NONE "\033[0m"
# define P_RED "\e[38;5;13m"
# define P_PINK "\e[38;5;204m"
# define P_PURPLE "\e[38;5;135m"
# define P_BLUE "\e[38;5;39m"
# define TURQUOISE "\e[38;5;36m"
# define P_GREEN "\e[38;5;119m"
# define YELLOW "\e[38;5;226m"
# define SUN "\e[38;5;208m"

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

/*
** IS
*/

int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_isspace(char c);
int					ft_what_alnum(int c);

/*
** WRITE
*/

void				ft_putchar(char c);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr(char const *s);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putnbr(int n);
void				ft_putnbr_fd(int n, int fd);
void				ft_putendl(char const *s);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putsign(int n);
void				ft_putsign_fd(int n, int fd);
void				ft_putnbrl(int n);
void				ft_putnbrl_fd(int n, int fd);
void				ft_putnbr_base(int nb, char *base);
void				ft_putstr_non_printable(char *str);
void				ft_putstr_replace(char *str, char c, char rep);
void				ft_putlst(t_list *list);

/*
** COLOR
*/

void				ft_putchar_c(char c, char *color);
void				ft_putstr_c(char const *s, char *color);
void				ft_putnbr_c(int n, char *color);
void				ft_putendl_c(char const *s, char *color);
void				ft_putnbrl_c(int n, char *color);

/*
** STR
*/

size_t				ft_strlen(const char *s);
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncpy(char *dst, const char *src, size_t n);
char				*ft_strstr(const char *haystack, const char *needle);
char				*ft_strcat(char *dst, const char *src);
char				*ft_strncat(char *dst, const char *src, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strnstr(const char *haystack, const char *needle,
		size_t len);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
int					ft_toupper(int c);
int					ft_tolower(int c);
char				*ft_strrev(char *str);

/*
** FUNCT PTR
*/

void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
void				ft_foreach(int *tab, int length, void (*f)(int));

/*
** MEM STR
*/

void				ft_bzero(void *s, size_t n);
char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
char				*ft_strdup(const char *s1);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
char				*ft_strctrim(char const *s, char c);
char				**ft_strsplit(char const *s, char c);

/*
** MEM
*/

void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
void				*ft_memcpy(void *dst, const void *src, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memchr(const void *s, int c, size_t n);
void				*ft_memset(void *b, int c, size_t len);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);

/*
** CALC - CONV
*/

void				ft_swap(int *a, int *b);
int					ft_factorial(int nb);
int					ft_sqrt(int n);
int					*ft_range(int min, int max);
int					ft_power(int n, int power);
int					ft_atoi(const char *str);
int					ft_atoi_base(char *str, char *base);
char				*ft_itoa(int n);
char				*ft_itoa_base(int value, int base);
char				*ft_convert_base(char *nbr, char *base_from, char *base_to);

/*
** LST
*/

t_list				*ft_lstnew(void const *content, size_t content_size);
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));

#endif
