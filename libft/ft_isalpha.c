/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 22:36:52 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 14:29:32 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int	ft_isalpha(int c)
{
	int res;

	res = ft_what_alnum(c);
	if (res == LOWCASE || res == UPCASE)
		return (TRUE);
	else
		return (FALSE);
}
