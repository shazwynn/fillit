/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 20:59:12 by algrele           #+#    #+#             */
/*   Updated: 2018/04/07 00:45:42 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int	ab_check_base(char *base)
{
	int	i;
	int j;

	i = 0;
	j = 0;
	if (!base || !base[i])
		return (0);
	while (base[i])
	{
		if (base[i] == '+' || base[i] == '-')
			return (0);
		i++;
	}
	if (i == 1)
		return (0);
	i = 0;
	while (base[i])
	{
		j = i + 1;
		while (base[j])
			if (base[i] == base[j++])
				return (0);
		i++;
	}
	return (1);
}

static int	ab_is_in_base(char c, char *base)
{
	int	i;

	i = 0;
	while (base[i])
	{
		if (c == base[i])
			return (i);
		i++;
	}
	return (-1);
}

static int	ab_check_str(char *str, char *base)
{
	int	i;

	i = 0;
	if (!str)
		return (0);
	while (str[i])
	{
		if (ab_is_in_base(str[i], base) == -1)
		{
			if (str[i] != ' ' && str[i] != '-' && str[i] != '+')
				return (0);
		}
		i++;
	}
	return (1);
}

int			ft_atoi_base(char *str, char *base)
{
	int	res;
	int i;
	int	i_base;
	int	s;

	i = 0;
	i_base = 0;
	res = 0;
	s = 0;
	if (!ab_check_base(base) || !ab_check_str(str, base))
		return (0);
	while (base[i_base])
		i_base++;
	while (str[s] && (str[s] == ' '))
		str = str + 1;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (str[i + s] && ab_is_in_base(str[i + s], base) != -1)
	{
		res = (res * i_base) + ab_is_in_base(str[i], base);
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}
