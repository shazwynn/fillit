/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 20:45:01 by algrele           #+#    #+#             */
/*   Updated: 2018/04/07 00:45:55 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int	cb_is_in_base(char c, char *base)
{
	int	i;

	i = 0;
	while (base[i])
	{
		if (c == base[i])
			return (i);
		i++;
	}
	return (-1);
}

static int	cb_atoi_base(char *str, char *base)
{
	int	res;
	int i;
	int	i_base;
	int	s;

	i = 0;
	i_base = 0;
	res = 0;
	s = 0;
	while (base[i_base])
		i_base++;
	while (str[s] && (str[s] == ' '))
		str = str + 1;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (str[i + s] && cb_is_in_base(str[i + s], base) != -1)
	{
		res = (res * i_base) + cb_is_in_base(str[i], base);
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}

static int	cb_create_array(long int num, char *base_to, char *array)
{
	int i;
	int	o_base;

	if (num < 0)
	{
		array[0] = '-';
		num = -num;
	}
	o_base = 0;
	while (base_to[o_base])
		o_base++;
	if (num >= o_base)
		cb_create_array(num / o_base, base_to, array);
	i = 0;
	while (array[i])
		i++;
	array[i] = base_to[num % o_base];
	return (i);
}

static char	*cb_itoa_base(long int num, char *base_to)
{
	char	*tmp;
	int		i;
	char	*res;

	tmp = (char *)malloc(sizeof(char) * (32 + 1));
	if (!tmp)
		return (NULL);
	i = -1;
	while (++i < 33)
		tmp[i] = '\0';
	i = cb_create_array(num, base_to, tmp);
	res = (char *)malloc(sizeof(char) * (i + 1));
	if (!res)
		return (NULL);
	i = 0;
	while (tmp[i])
	{
		res[i] = tmp[i];
		i++;
	}
	res[i] = '\0';
	free(tmp);
	return (res);
}

char		*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	int		i_base;
	int		o_base;
	int		res_decimal;
	char	*res;

	i_base = 0;
	o_base = 0;
	while (base_from[i_base])
		i_base++;
	while (base_to[o_base])
		o_base++;
	res_decimal = cb_atoi_base(nbr, base_from);
	res = cb_itoa_base(res_decimal, base_to);
	return (res);
}
