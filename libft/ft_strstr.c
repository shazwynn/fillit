/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 15:59:54 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 16:59:55 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** locates the first occurence of needle in haystack. if needle is empty, ret
** haystack. if no occurence of needle in haystackm ret NULL. else, ret a ptr
** to the first occurence of needle.
*/

char	*ft_strstr(const char *haystack, const char *needle)
{
	int		i;
	int		j;

	i = 0;
	if (!needle[i])
		return ((char *)haystack);
	else
	{
		while (haystack[i])
		{
			j = 0;
			while (haystack[i + j] == needle[j])
			{
				if (needle[j + 1] == '\0')
					return ((char *)haystack + i);
				j++;
			}
			i++;
		}
	}
	return (NULL);
}
