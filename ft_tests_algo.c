/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tests_algo.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 19:36:50 by algrele           #+#    #+#             */
/*   Updated: 2018/04/11 00:52:31 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_display_tetri(int tetris[4][2])
{
	int	i;

	i = 0;
	if (tetris)
	{
		while (i < 4)
		{
			ft_putnbr(tetris[i][0]);
			ft_putchar(';');
			ft_putnbr(tetris[i][1]);
			if (i < 3)
				ft_putstr(" | ");
			i++;
		}
		ft_putchar('\n');
	}
	else
		ft_putendl("NULL");
}

void	ft_display_list(t_tetris *list)
{
	ft_putendl("\n--- display_list (Tetri nb : ligne ; colonne) ---");
	while (list)
	{
		ft_putnbr(list->order);
		ft_putstr(" : ");
		ft_display_tetri(list->tetris);
		list = list->next;
	}
}

/*
void	ft_display_grid(t_tetris *list)
{

}
*/
