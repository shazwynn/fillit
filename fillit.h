#ifndef FILLIT_H
# define FILLIT_H

# include "libft.h"
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

typedef struct	s_tetris
{
	int				order;
	int				shape[4];
	int				tetris[4][2];
	struct s_tetris	*next;
}				t_tetris;

typedef	int		t_bool;

char			*ft_read_file(int fd);
t_tetris		*ft_stock_tetris(char *file, int *ntetris);
void			ft_list_push_back(t_tetris **begin_list, t_tetris *elem);
t_tetris		*ft_create_elem(int ntetris);
int				ft_minsquare(int ntetris);
void			ft_convert_tetris(t_tetris *begin_list);
void			ft_display_list(t_tetris *list);
void			ft_display_tetri(int tetris[4][2]);
int				ft_backtracking(t_tetris *begin_list, t_tetris *next, int sq_size);
void			ft_display_result(t_tetris *begin_list, int sq_size);

# define BUFF_SIZE 29000

#endif
