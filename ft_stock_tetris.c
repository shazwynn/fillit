/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stock_tetris.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triou <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 11:18:23 by triou             #+#    #+#             */
/*   Updated: 2018/04/10 22:18:05 by triou            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static t_bool	ft_is_valid_shape(t_tetris *new)
{
	int	i;
	int	j;
	int ret;

	i = -1;
	ret = 0;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			if (i != j && (new->shape[i] / 4 == new->shape[j] / 4)
					&& ((new->shape[i] % 4 + 1 == new->shape[j] % 4)
					|| (new->shape[i] % 4 - 1 == new->shape[j] % 4)))
				ret++;
			if (i != j && (new->shape[i] % 4 == new->shape[j] % 4)
				&& ((new->shape[i] / 4 + 1 == new->shape[j] / 4)
				|| (new->shape[i] / 4 - 1 == new->shape[j] / 4)))
				ret++;
		}
	}
	if (ret != 6 && ret != 8)
		return (0);
	return (1);
}

static t_bool	ft_add_tetris(int *sharp, int *pos, t_tetris *new)
{
	(*sharp)++;
	(*pos)++;
	if (*sharp > 4)
		return (0);
	else if (*sharp == 4)
	{
		new->shape[*sharp - 1] = *pos;
		if (!ft_is_valid_shape(new))
			return (0);
	}
	else if (*sharp > 1)
		new->shape[*sharp - 1] = *pos;
	else
		new->shape[*sharp - 1] = *pos;
	return (1);
}

static t_bool	ft_is_valid_grid(char c, int *point, int *nline, int *pos)
{
	if (c == '.')
	{
		(*point)++;
		(*pos)++;
		if (*point > 12)
			return (0);
		return (1);
	}
	else if (c == '\n')
	{
		(*nline)++;
		if ((*pos % 4) != 3 || *nline > 4)
			return (0);
		return (1);
	}
	else if (c == '#')
		return (1);
	return (0);
}

static char		*ft_is_valid_file(char *file, t_tetris *new)
{
	int	point;
	int	sharp;
	int	nline;
	int	pos;

	point = 0;
	sharp = 0;
	nline = 0;
	pos = -1;
	while (*file && pos < 15)
	{
		if (!ft_is_valid_grid(*file, &point, &nline, &pos))
			return (NULL);
		if (*file == '#' && !ft_add_tetris(&sharp, &pos, new))
			return (NULL);
		file++;
	}
	if (sharp == 4 && *file == '\n')
		return (++file);
	return (NULL);
}

t_tetris		*ft_stock_tetris(char *file, int *ntetris)
{
	t_tetris	*begin_list;
	t_tetris	*new;

	*ntetris = 0;
	begin_list = NULL;
	while (*file)
	{
		if (*ntetris >= 26
			|| !(new = ft_create_elem(*ntetris))
			|| !(file = ft_is_valid_file(file, new)))
			return (NULL);
		ft_list_push_back(&begin_list, new);
		if (*file == '\n' && (*(file + 1) == '.' || *(file + 1) == '#'))
			file++;
		else if (*file)
			return (NULL);
		(*ntetris)++;
	}
	return (begin_list);
}
