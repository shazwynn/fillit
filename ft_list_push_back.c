#include "fillit.h"

void	ft_list_push_back(t_tetris **begin_list, t_tetris *elem)
{
	t_tetris *list;

	if(*begin_list == NULL)
	{
		*begin_list = elem;
		return ;
	}
	list = *begin_list;
	while (list->next)
		list = list->next;
	list->next = elem;
}
