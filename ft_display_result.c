/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_result.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 00:00:41 by algrele           #+#    #+#             */
/*   Updated: 2018/04/11 00:37:57 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void	ft_increment_pos(int *x, int *y, int sq_size)
{
	if (*y == sq_size - 1)
	{
		*y = 0;
		*x = *x + 1;
	}
	else
		*y = *y + 1;
}

static int	ft_find_let_pos(int x, int y, int tetris[4][2])
{
	int	i;

	i = 0;
	while (i < 4)
	{
		if (tetris[i][0] == x && tetris[i][1] == y)
			return (1);
		i++;
	}
	return (0);
}

void		ft_display_result(t_tetris *begin_list, int sq_size)
{
	int			x;
	int			y;
	char		c;
	t_tetris	*curr;

	x = 0;
	y = 0;
	c = '.';
	while (y < sq_size && x < sq_size)
	{
		curr = begin_list;
		while (curr)
		{
			if (ft_find_let_pos(x, y, curr->tetris))
				c = curr->order + 'A';
			curr = curr->next;
		}
		ft_putchar(c);
		if (c != '.')
			c = '.';
		if (y == sq_size - 1)
			ft_putchar('\n');
		ft_increment_pos(&x, &y, sq_size);
	}
}
