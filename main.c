#include "fillit.h"

int	main(int argc, char **argv)
{
	int			fd;
	char		*file;
	t_tetris	*begin_list;
	int			ntetris;
	int			sq_size;
	t_tetris	*prev;

	if (argc != 2)
	{
		ft_putstr("Invalid number of arguments.\n");
		return (1);
	}
	if(open(argv[1], O_DIRECTORY) != -1)
	{
		ft_putstr("Impossible d'ouvrir un dossier.\n");
		return (1);
	}
	if((fd = open(argv[1], O_RDONLY)) < 0)
	{
		ft_putstr("Impossible d'ouvrir le fichier.\n");
		return (1);
	}
	if (!(file = ft_read_file(fd)))
	{
		ft_putstr("Memoire disponible insuffisante.\n");
		return (1);
	}
	ft_putstr(file);
	if (!(begin_list = ft_stock_tetris(file, &ntetris)))
	{
		ft_putstr("Invalid grid.\n");
		free(file);
		return (0);
	}
	ft_convert_tetris(begin_list);
	ft_display_list(begin_list);
	ft_putstr("---------------------\n");
	free(file);
	if (close(fd) < 0)
		return (1);
	sq_size = ft_minsquare(ntetris);
	ft_putstr("Min square = ");
	ft_putnbr(sq_size);
	ft_putchar('\n');
	while (!ft_backtracking(begin_list, begin_list, sq_size))
			sq_size++;
	ft_display_list(begin_list);
	ft_putstr("Square size = ");
	ft_putnbr(sq_size);
	ft_putstr("\n-----------------------\n");
	ft_display_result(begin_list, sq_size);
	while (begin_list)
	{
		prev = begin_list;
		begin_list = begin_list->next;
		free(prev);
	}
	return (0);
}
