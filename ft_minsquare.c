/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt_minsquare.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triou <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 19:16:30 by triou             #+#    #+#             */
/*   Updated: 2018/04/09 19:20:29 by triou            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_minsquare(int ntetris)
{
	int	nblocks;
	int	sqrt;

	sqrt = 1;
	nblocks = 4 * ntetris;
	while (sqrt <= nblocks / sqrt)
		sqrt++;
	return (sqrt);
}

