/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_backtracking.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triou <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 15:57:19 by triou             #+#    #+#             */
/*   Updated: 2018/04/11 01:09:55 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h> // delete me

static void	ft_convert_pos(t_tetris *next, int tmp[4][2], int pos, int sq_size)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		tmp[i][0] = next->tetris[i][0] + pos / sq_size;
		tmp[i][1] = next->tetris[i][1] + pos % sq_size;
		i++;
	}
}

static int ft_tetriscmp(int tetris[4][2], int tmp[4][2])
{
	int i;
	int	j;

	i = 0;
//	ft_putstr("comparing these two :\n");
//	ft_display_tetri(tetris);
//	ft_display_tetri(tmp);
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if (tetris[i][0] == tmp[j][0] && tetris[i][1] == tmp[j][1])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

static int	ft_is_inside_bound(int tmp[4][2], int sq_size)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		if (tmp[i][1] < 0 || tmp[i][1] >= sq_size
				|| tmp[i][0] < 0 || tmp[i][1] >= sq_size)
			return (0);
		i++;
	}
	return (1);
}

static int	ft_is_free(t_tetris *begin_list, t_tetris *next, int pos, int sq_size)
{
	t_tetris	*list;
	int			tmp[4][2];

	list = begin_list;
	//	printf("testing pos %d for tetri %c ", pos, 'A' + next->order); // delete me
/*	ft_putstr("tetriminos ");
	ft_putchar(next->order + 'A');
	ft_putchar('\n');
	ft_display_tetri(next->tetris);
	ft_putendl("convert");
	*/
	ft_convert_pos(next, tmp, pos, sq_size);
	ft_display_tetri(tmp);
	if (!ft_is_inside_bound(tmp, sq_size))
		return (0);
	while (list != next)
	{
		if (!ft_tetriscmp(list->tetris, tmp))
			return (0);
		list = list->next;
	}
	return (1);
}

static void	ft_set_pos(int pos, t_tetris *next, int sq_size)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		next->tetris[i][0] += pos / sq_size;
		next->tetris[i][1] += pos % sq_size;
		i++;
	}
}

static void	ft_reset_pos(t_tetris *next)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		next->tetris[i][0] -= next->tetris[0][0];
		next->tetris[i][1] -= next->tetris[0][1];
		i++;
	}
}

int			ft_backtracking(t_tetris *begin_list, t_tetris *next, int sq_size)
{
	int	pos;

	if (!next)
		return (1);
	pos = 0;
	while (pos < sq_size * sq_size)
	{
		if (ft_is_free(begin_list, next, pos, sq_size))
		{
			printf(" --> ok\n");
			ft_set_pos(pos, next, sq_size);
			if (ft_backtracking(begin_list, next->next, sq_size))
				return (1);
			else
				ft_reset_pos(next);
		}
		pos++;
	}
	return (0);
}
