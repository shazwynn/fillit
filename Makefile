CC = gcc
CFLAGS = -g -fsanitize=address -Wall -Werror -Wextra
NAME = fillit
SDIR = .
IDIR = .
ODIR = .
SRCS =	main.c \
		ft_read_file.c \
		ft_create_elem.c \
		ft_list_push_back.c \
		ft_stock_tetris.c \
		ft_minsquare.c \
		ft_tests_algo.c \
		ft_convert_tetris.c \
		ft_backtracking.c \
		ft_display_result.c
LIB = ft
_INCLUDES = fillit.h
INClUDES = $(patsubst %, $(IDIR)/%, $(_INCLUDES))
_OBJ = $(SRCS:.c=.o)
OBJ = $(patsubst %, $(ODIR)/%, $(_OBJ))

all : $(NAME)

$(NAME) : $(OBJ)
	@$(CC) $(CFLAGS) -o $@ $^ -L$(IDIR) -l$(LIB)
	@echo "\033[1;34mFillit \033[1;35mhas been created \033[1;5;32msuccessfully.\033[0m"
	
$(ODIR)/%.o : $(SDIR)/%.c $(INCLUDES)
	@$(CC) $(CFLAGS) -o $@ -c $< -I$(IDIR)

clean : 
	@rm -f $(OBJ)

fclean : clean
	@rm -f $(NAME)

re : fclean all
